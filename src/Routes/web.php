<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'blog.', 'namespace' => '\\Pashynskyi\\Blog\\Http\\Controllers', 'middleware' => ['web']], function () {
    Route::get(LaravelLocalization::transRoute('blog::locale.routes.posts'), 'BlogController@index')->name('index');
    Route::get(LaravelLocalization::transRoute('blog::locale.routes.category'), 'BlogController@category')->name('category');
    Route::get(LaravelLocalization::transRoute('blog::locale.routes.tag'), 'BlogController@tag')->name('tag');
    Route::get(LaravelLocalization::transRoute('blog::locale.routes.archive'), 'BlogController@archive')->name('archive');
    Route::get(LaravelLocalization::transRoute('blog::locale.routes.post.show'), 'BlogController@show')->name('show');
});

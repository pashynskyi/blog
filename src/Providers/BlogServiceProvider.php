<?php

namespace Pashynskyi\Blog\Providers;

use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    const PATH_MIGRATIONS = __DIR__ . '/../Database/Migrations';
    const PATH_TRANSLATIONS = __DIR__ . '/../Resources/Lang';
    const PATH_ROUTES = __DIR__ . '/../Routes/web.php';
    const PATH_CONFIG = __DIR__ . '/../Config/blog.php';

    public function register()
    {
        //
    }

    public function boot()
    {
        $this->loadMigrationsFrom(self::PATH_MIGRATIONS);

        $this->loadTranslationsFrom(self::PATH_TRANSLATIONS, 'blog');

        $this->loadRoutesFrom(self::PATH_ROUTES);

        if ($this->app->runningInConsole()) {
            $this->publishes([
                self::PATH_TRANSLATIONS => $this->app->langPath('vendor/blog'),
            ], 'locales');

            $this->publishes([
                self::PATH_MIGRATIONS => $this->app->databasePath('vendor/blog'),
            ], 'migrations');

            $this->publishes([
                self::PATH_CONFIG => $this->app->configPath('blog.php'),
            ], 'config');
        }
    }
}

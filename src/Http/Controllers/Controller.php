<?php

namespace Pashynskyi\Blog\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use View;

class Controller extends BaseController
{
    protected function response($indexViewPath, $gridViewPath, $viewData)
    {
        return request()->ajax() ? ['success' => true, 'html' => View::make($gridViewPath, $viewData)->render()] : view($indexViewPath, $viewData);
    }
}

<?php

namespace Pashynskyi\Blog\Http\Controllers;

use Pashynskyi\Blog\Models\Category;
use Pashynskyi\Blog\Models\Localization;
use Pashynskyi\Blog\Models\Post;
use Pashynskyi\Blog\Models\Tag;
use Carbon\Carbon;

class BlogController extends Controller
{
    protected $perPage;
    protected $search;
    protected $categoryId;
    protected $tagId;
    protected $archiveDate;

    protected $allCategories = [];
    protected $allTags = [];
    protected $archivePosts = [];

    public function init()
    {
        $request = request();
        $this->perPage = $request->get('per_page', 4);
        $this->search = $request->get('search');

        $this->allCategories = Category::withLocalization()->withCount('posts')->having('posts_count', '>', 0)->get();

        $this->allTags = Tag::withCount('posts')->having('posts_count', '>', 0)->get();
        $this->archivePosts = Post::selectArchive();
    }

    public function tag($tagSlug)
    {
        $tag = Tag::__firstOrFail('slug', $tagSlug);
        $this->tagId = $tag->id;
        return $this->index();
    }

    public function category($categorySlug)
    {
        $category = Category::__firstOrFail('slug', $categorySlug);
        $this->categoryId = $category->id;
        return $this->index();
    }

    public function archive($archiveDate)
    {
        $validator = \Validator::make([
            'date' => $archiveDate,
        ], [
            'date' => 'regex:/^\d{4}\-\d{2}/i',
        ]);

        if ($validator->fails()) {
            abort(404);
        }

        $this->archiveDate = $archiveDate;
        return $this->index();
    }

    public function index()
    {
        $this->init();

        $postQuery = Post::withLocalization()->with('tags', 'category')
            ->categoryId($this->categoryId)
            ->archiveDate($this->archiveDate)
            ->search($this->search, ['title', 'body', 'body_preview'], Localization::class);

        if ($this->tagId) {
            $postQuery->whereHas('tags', function ($query) {
                $query->where('tag_id', $this->tagId);
            });
        }

        $posts = $postQuery->latest()->paginate($this->perPage);
        $postsCount = $posts->total();

        return view('vendor.blog.index', array_merge($this->getGeneralData(), compact('posts', 'postsCount')));
    }

    public function show($postSlug)
    {
        $this->init();

        $post = Post::__firstOrFail('slug', $postSlug);

        return view('vendor.blog.show', array_merge($this->getGeneralData(), compact('post')));
    }

    private function getGeneralData()
    {
        return [
            'archivePosts' => $this->archivePosts,
            'allCategories' => $this->allCategories,
            'allTags' => $this->allTags,
            'search' => $this->search,
            'categoryId' => $this->categoryId,
            'tagId' => $this->tagId,
            'archiveDate' => $this->archiveDate,
        ];
    }
}

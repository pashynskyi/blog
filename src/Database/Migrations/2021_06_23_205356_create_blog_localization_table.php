<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_localization', function (Blueprint $table) {
            $table->id();

            $table->string('field');
            $table->string('language');
            $table->text('value');
            $table->string('localizable_type');
            $table->unsignedBigInteger('localizable_id')->index();

            $table->unique(['field', 'localizable_type', 'localizable_id', 'language'], 'blog_localization_unique');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_localization');
    }
};

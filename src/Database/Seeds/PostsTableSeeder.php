<?php

namespace Pashynskyi\Blog\Database\Seeds;

use Pashynskyi\Blog\Models\Post;
use Pashynskyi\Blog\Models\Tag;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    public function run()
    {
        Post::factory()->count(30)->create()->each(function ($post) {
            $tags = Tag::inRandomOrder()->limit(rand(2, 6))->get('id')->pluck(['id'])->toArray();
            $post->tags()->attach($tags);
        });
    }
}

<?php

namespace Pashynskyi\Blog\Database\Seeds;

use Pashynskyi\Blog\Models\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    public function run()
    {
        Tag::factory()->count(40)->create();
    }
}

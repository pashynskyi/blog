<?php

namespace Pashynskyi\Blog\Database\Seeds;

use Pashynskyi\Blog\Models\Category;
use Pashynskyi\Blog\Models\Localization;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        Category::factory()->count(8)->create()->each(function ($category) {
            $localization = new Localization();
            $localization->language = 'ru';
            $localization->field = 'name';
            $localization->value = $category->name . ' ru';
            $category->localization()->save($localization);
        });

        $category = Category::find(1)->first();

        $localization = new Localization();
        $localization->language = 'ru';
        $localization->field = 'name';
        $localization->value = 'Russian localized category name';
        $category->localization()->update($localization);

        $localization = new Localization();
        $localization->language = 'en';
        $localization->field = 'name';
        $localization->value = 'English localized category name';
        $category->localization()->save($localization);
    }
}

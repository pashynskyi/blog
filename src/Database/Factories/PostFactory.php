<?php

namespace Pashynskyi\Blog\Database\Factories;

use Pashynskyi\Blog\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;
use Pashynskyi\Blog\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PostFactory extends Factory
{
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = $this->faker->sentence;

        return [
            'category_id' => Category::inRandomOrder()->first()->id,
            'title' => $title,
            'slug' => Str::slug($title),
            'body' => '<p>' . $this->faker->text . $this->faker->text . $this->faker->text . $this->faker->text . '</p>',
            'body_preview' => $this->faker->text,
            'image' => $this->faker->imageUrl(820, 481),
            'meta_title' => $title,
            'meta_keywords' => $this->faker->sentence,
            'meta_description' => $this->faker->sentence,
            'created_at' => $this->faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now'),
        ];
    }
}

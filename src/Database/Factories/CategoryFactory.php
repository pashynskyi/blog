<?php

namespace Pashynskyi\Blog\Database\Factories;

use Pashynskyi\Blog\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CategoryFactory extends Factory
{
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $name = ucfirst($this->faker->unique()->word);

        return [
            'name' => $name,
            'slug' => Str::slug($name),
        ];
    }
}

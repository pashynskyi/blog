<?php

namespace Pashynskyi\Blog\Models;

use Pashynskyi\Blog\Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Pashynskyi\Localizable\Traits\Localizable;

class Category extends Model
{
    use HasFactory, Localizable;

    protected $table = 'blog_categories';

    protected $fillable = [
        'name',
        'slug'
    ];

    protected static function newFactory()
    {
        return new CategoryFactory();
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    protected static function getLocalizationClass()
    {
        return Localization::class;
    }
}

<?php

namespace Pashynskyi\Blog\Models;

use Pashynskyi\Blog\Database\Factories\PostFactory;
use Pashynskyi\Blog\Models\Traits\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Pashynskyi\Localizable\Traits\Localizable;
use Str;
use DB;

class Post extends Model
{
    use HasFactory, Searchable, Localizable;

    protected $table = 'blog_posts';

    protected $fillable = [
        'slug',
        'title',
        'body',
        'body_preview',
        'image',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'category_id',
    ];

    protected static function newFactory()
    {
        return new PostFactory();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'blog_post_tag', 'post_id', 'tag_id')->withTimestamps();
    }

    public static function scopeCategoryId(Builder $query, $categoryId)
    {
        if (!empty($categoryId)) {
            $query->where('category_id', $categoryId);
        }
        return $query;
    }

    public static function scopeArchiveDate(Builder $query, $archiveDate)
    {
        if (!empty($archiveDate)) {
            list($year, $month) = explode('-', $archiveDate);
            $query
                ->whereYear('created_at', '=', $year)
                ->whereMonth('created_at', '=', $month);
        }
        return $query;
    }

    public function showBodyPreview($length = 500)
    {
        return !empty($this->body_preview) ? $this->__('body_preview') : Str::substr($this->__('body'), 0, $length);
    }

    public static function selectArchive()
    {
        // SELECT COUNT(*), posts_count, DATE_FORMAT(created_at, '%Y %M') as d FROM `blog_posts` GROUP BY d ORDER BY d DESC

        $dateFormat = "DATE_FORMAT(created_at, '%Y-%m')";

        return DB::table('blog_posts')->select(
            DB::raw($dateFormat . ' AS date'),
            DB::raw('COUNT(*) AS count')
        )->groupBy('date')->orderBy('date', 'DESC')->get();
    }

    protected static function getLocalizationClass()
    {
        return Localization::class;
    }
}

<?php

namespace Pashynskyi\Blog\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Searchable
{
    public static function scopeSearch(Builder $query, $search, array $fields, $localizationClass = null)
    {
        if (empty($search) || empty($fields)) {
            return $query;
        }

        $query->where(\DB::raw('1'));

        foreach ($fields as $field) {
            @list($firstPart, $secondPart) = explode('.', $field);
            $relationName = isset($secondPart) ? $firstPart : null;
            $fieldName = $relationName ? $secondPart : $firstPart;

            if (!$relationName) {
                $query->orWhere($fieldName, 'like', "%{$search}%");
            }
            else {
                $query->orWhereHas($relationName, function ($query) use ($search, $fieldName) {
                    $query->where($fieldName, 'like', "%{$search}%");
                });
            }
        }

        if (!empty($localizationClass) && class_exists($localizationClass)) {
            $ids = [];
            foreach ($fields as $fieldData) {
                @list($field, $relationName) = explode('.', $fieldData);

                $localizationIds = $localizationClass::select('localizable_id')->whereHasMorph('localizable', [static::class], function($query) use ($search, $field) {
                    $query->where('value', 'like', "%{$search}%")->where('field', $field);
                })->pluck('localizable_id')->toArray();
                $ids = array_merge($ids, $localizationIds);
            }

            if (!empty($ids)) {
                $query->orWhereIn('id', $ids);
            }
        }

        return $query;
    }
}

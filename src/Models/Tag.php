<?php

namespace Pashynskyi\Blog\Models;

use Pashynskyi\Blog\Database\Factories\TagFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Pashynskyi\Localizable\Traits\Localizable;

class Tag extends Model
{
    use HasFactory, Localizable;

    protected $table = 'blog_post_tags';

    protected $fillable = [
        'name',
        'slug'
    ];

    protected static function newFactory()
    {
        return new TagFactory();
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'blog_post_tag','tag_id', 'post_id')->withTimestamps();
    }

    protected static function getLocalizationClass()
    {
        return Localization::class;
    }
}

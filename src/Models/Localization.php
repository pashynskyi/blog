<?php

namespace Pashynskyi\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Localization extends Model
{
    protected $table = 'blog_localization';

    public function localizable()
    {
        return $this->morphTo();
    }
}

<?php
return [
    'routes' => [
        'posts' => 'блог',
        'category' => '/блог/категорія/{categorySlug}',
        'tag' => '/блог/тег/{tagSlug}',
        'archive' => '/блог/архів/{archiveDate}',
        'post' => [
            'show' => '/стаття/{postSlug}',
        ],
    ],

    'home' => 'Головна',
    'blog' => 'Блог',
    'search' => 'Пошук',
    'categories' => 'Категорії',
    'archive' => 'Архів',
    'tags' => 'Теги',
    'continue_reading' => 'Детальніше',
];

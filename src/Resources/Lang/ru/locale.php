<?php
return [
    'routes' => [
        'posts' => 'блог',
        'category' => '/блог/категория/{categorySlug}',
        'tag' => '/блог/тэг/{tagSlug}',
        'archive' => '/блог/архив/{archiveDate}',
        'post' => [
            'show' => '/статья/{postSlug}',
        ],
    ],

    'home' => 'Главная',
    'blog' => 'Блог',
    'search' => 'Поиск',
    'categories' => 'Категории',
    'archive' => 'Архив',
    'tags' => 'Теги',
    'continue_reading' => 'Подробнее',
];
